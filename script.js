function Answer() {
    const answers = ['It is certain.', 'It is decidedly so.', 'Without a doubt.', 'Yes – definitely.', 'You may rely on it.', 
    'As I see it, yes.','Most likely.', 'Outlook good.', 'Yes.', 'Signs point to yes.', 'Reply hazy, try again.', 
    'Ask again later.', 'Better not tell you now.', 'Cannot predict now.', 'Concentrate and ask again.', "Don't count on it.", 
    'My reply is no.', 'My sources say no.', 'Outlook not so good.', 'Very doubtful.']

    let random =  Math.floor((19 - 0) * Math.random())

    document.getElementById("loader").id = "answer";
    const result = document.querySelector('#answer p').textContent = `${answers[random]}`
}

const button = document.getElementById('AskBtn')
button.addEventListener('click', loader)

function loader() {
    document.getElementById("answer").id = "loader";
    setTimeout(Answer, 2000);
}